import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamineStageComponent } from './examine-stage.component';

describe('ExamineStageComponent', () => {
  let component: ExamineStageComponent;
  let fixture: ComponentFixture<ExamineStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamineStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamineStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
