import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngageStageComponent } from './engage-stage.component';

describe('EngageStageComponent', () => {
  let component: EngageStageComponent;
  let fixture: ComponentFixture<EngageStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngageStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngageStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
