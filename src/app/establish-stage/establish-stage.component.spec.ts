import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishStageComponent } from './establish-stage.component';

describe('EstablishStageComponent', () => {
  let component: EstablishStageComponent;
  let fixture: ComponentFixture<EstablishStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstablishStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
