import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducateStageComponent } from './educate-stage.component';

describe('EducateStageComponent', () => {
  let component: EducateStageComponent;
  let fixture: ComponentFixture<EducateStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducateStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducateStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
