import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressStatusBarComponent } from './progress-status-bar.component';

describe('ProgressStatusBarComponent', () => {
  let component: ProgressStatusBarComponent;
  let fixture: ComponentFixture<ProgressStatusBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressStatusBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressStatusBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
