import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicianNotesComponent } from './clinician-notes.component';

describe('ClinicianNotesComponent', () => {
  let component: ClinicianNotesComponent;
  let fixture: ComponentFixture<ClinicianNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicianNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicianNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
