import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterventionModuleComponent } from './intervention-module.component';

describe('InterventionModuleComponent', () => {
  let component: InterventionModuleComponent;
  let fixture: ComponentFixture<InterventionModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterventionModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
