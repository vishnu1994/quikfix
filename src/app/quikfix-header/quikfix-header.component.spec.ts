import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuikfixHeaderComponent } from './quikfix-header.component';

describe('QuikfixHeaderComponent', () => {
  let component: QuikfixHeaderComponent;
  let fixture: ComponentFixture<QuikfixHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuikfixHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuikfixHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
